package com.javaeeeee.dwstart.resources;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.javaeeeee.dwstart.core.Greeting;

import javax.swing.text.StringContent;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by kiidbrian on 5/27/16.
 */
@Path("/hello")
public class HelloResource {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getGreeting() {
        return "Hello world!";
    }

    @Path("/hello_json")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Greeting getJSONGreeting() {
        return new Greeting("Hello World!");
    }
}


