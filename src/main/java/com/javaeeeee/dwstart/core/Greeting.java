package com.javaeeeee.dwstart.core;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kiidbrian on 5/27/16.
 */
public class Greeting {
    @JsonProperty
    private String greeting;

    public Greeting() {
    }

    public Greeting(String greeting) {
        this.greeting = greeting;
    }

    public String getGreeting() {
        return greeting;
    }
}
